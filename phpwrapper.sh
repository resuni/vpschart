#!/bin/bash

# This is a wrapper for the scripts in the "php/" directory.
# The purpose of this script is to simplify crontab and make testing easier.

gitpush () {
 if [ $EXIT != 0 ]; then
  exit $EXIT
 fi
 if [ $NOGIT == 1 ]; then
  exit
 else
  git add public/rows/$PROVIDERSHORT.txt
  git commit -m "Auto-commit for $PROVIDERLONG `date +%Y%m%d`"
  git push
 fi
}

git pull -q

if [ "$2" == "nogit" ]; then
 NOGIT=1
else
 NOGIT=0
fi

case "$1" in
 digitalocean)
  PROVIDERSHORT="digitalocean"
  PROVIDERLONG="Digital Ocean"
  php php/digitalocean.php
  EXIT=$?
  gitpush
  ;;

 linode)
  PROVIDERSHORT="linode"
  PROVIDERLONG="Linode"
  php php/linode.php
  EXIT=$?
  gitpush
  ;;

 vultr)
  PROVIDERSHORT="vultr"
  PROVIDERLONG="Vultr"
  php php/vultr.php
  EXIT=$?
  gitpush
  ;;

 *)
  echo "Invalid argument: $1"
  echo "Usage: bash phpwrapper.sh <provider> [nogit]"
  exit 10
  ;;
esac

exit 0
