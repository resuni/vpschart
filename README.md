# vpschart

Website that compares plans and locations from major VPS providers.

This website is very much a work in progress. There are many more features and providers to come!

Breakdown of how this website works (or will work when completely functional):

* A clone of this repository sits on a Linux box somewhere where a cron job will run the scripts in the "php/" directory on a daily basis. 
* These PHP scripts download new location and plan data from the VPS provider's respective API.
* The PHP scripts check against locally saved data in the "compare/" directory. These JSON files are excluded using .gitignore, since there is no reason for them to also be uploaded to the GitHub repository. This is why the "compare/" directory is empty when viewed on GitHub.
* If the data the script downloaded is different from the existing data in the JSON file, three things will happen:
  1. The respective JSON file will be overwritten with the new data for the PHP script to check against again the next time it is run.
  2. New table rows will be generated and stored in the appropriate TXT file located in the "public/rows/" directory.
  3. Git will commit the updated TXT file and push the update to this repository.

Important notes:

* PHP scripts must be executed from the root directory of this repository.
* Store API keys in "php/.env". While the ".env" file itself is in .gitignore, an "env.template" file can be found in the "php/" directory for reference. 
* A Vagrant configuration has been included for testing.
