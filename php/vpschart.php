<?php

require("curl.php");

class vpschart {

 public $provider = null;
 public $providerShort = null;
 private $env = null;
 public $linodeEnv = null;
 public $apikey = null;
 public $response = null;
 public $response_code = null;
 public $new = [];

 public function __construct (string $provider) {
  $this->provider = $provider;
  $this->providerShort = strtolower($provider);
  $this->providerShort = str_replace(' ', '', $this->providerShort);

  // Exit if .env does not exist
  if (file_exists("php/.env")) {
   $this->env = json_decode(file_get_contents("php/.env"));
   $this->apikey = $this->env->{$this->providerShort};
  } else {
   echo(".env file does not exist");
   exit(254);
  }

  // Linode doesn't use a POST request to send the API key
  // This is a workaround for that
  $this->linodeEnv = $this->env->linode;

 }

 public function get (string $url) {
  $request = new curl();
  $request = $request->get($url);
  $this->response = $request[0];
  $this->response_code = $request[1];
 }

 public function keyget (string $url) {
  $request = new curl();
  $request = $request->keyget($url, $this->apikey);
  $this->response = $request[0];
  $this->response_code = $request[1];
 }

 // Function to compare new data with existing data.
 // If new data is different, overwrite existing data with new data.
 // Also returns true if data is different; false if not.
 public function comparedata (string $data, string $type) {
  if ($data != @file_get_contents("compare/$type/$this->providerShort.json")) {
   echo "$type: New data!\n";
   file_put_contents("compare/$type/$this->providerShort.json", $data);
   $this->new[$type] = 1;
  } else {
   $this->new[$type] = 0;
  }
 }

 // Function that determines latitude and longitude of a location using Nominatim
 // http://wiki.openstreetmap.org/wiki/Nominatim
 public function latlon (array $regionsList) {
  // Instansiate new curl object so we can make requests to Nominatim
  $nom = new curl();

  // Loop through array of regions to find their latitude & longitude
  $arraykey = 0;
  foreach($regionsList as $rawRegion) {

   // Strip numbers from the end of location names (if any)
   $rawRegion = preg_split("/\d$/", $rawRegion);

   // Percent-encode the region name so we can pass it in a URL
   $region = rawurlencode($rawRegion[0]);

   // Limit to 1 request per second, otherwise Nominatim will block us
   sleep(1);

   // Make the request
   $latlon = $nom->get(
    "https://nominatim.openstreetmap.org/search/$region?email={$this->env->nominatim}&format=jsonv2&accept-language=en-US"
   );

   // Pull latitude & longitude from response and add to array
   $latlon = json_decode($latlon[0], 1);
   $coords[$arraykey] = array(
    "reg" => $rawRegion[0],
    "lat" => $latlon[0]["lat"],
    "lon" => $latlon[0]["lon"],
   );

   // Increment array key and do it again
   $arraykey++;
  }
  return $coords;
 }
 
 // Function that defines table row entries.
 public function tablerows (
  string $url,		// URL to provider's website
  string $monthlyPrice,
  string $vcpus,
  string $memory,
  string $disk
 ) {
  $row = array(
   "provider" => "<a href='$url'><img src='icons/$this->providerShort.png' class='vpsicon'> $this->provider</a>",
   "monthlyPrice" => $monthlyPrice,
   "vcpu" => $vcpus,
   "ram" => $memory,
   "disk" => $disk
  );
  return $row;
 }
 
}
?>
