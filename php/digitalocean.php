<?php

/// Classes and includes

require_once("vpschart.php");
require_once("curl.php");

$o = new vpschart("Digital Ocean");

/// Download and check data

// Download plan data
$oPlan = new curl();
$oPlan->keyget("https://api.digitalocean.com/v2/sizes", $o->apikey);
if ($oPlan->response_code != 200) exit(20);

// Download region data
$oRegion = new curl();
$oRegion->keyget("https://api.digitalocean.com/v2/regions", $o->apikey);
if ($oRegion->response_code != 200) exit(20);

// Compare new data with existing data
$o->comparedata($oPlan->response, "plans");
$o->comparedata($oRegion->response, "regions");

/// Rebuild plans data if plans data changed

if ($o->new["plans"]) {
 
 // Convert JSON data to array
 $oPlan->response = json_decode($oPlan->response, true);

 // Trim unnecessary information from beginning of array
 $oPlan->response = $oPlan->response["sizes"];

 // Re-key array
 foreach($oPlan->response as $key) {
  $plansList[$key["slug"]] = $key;
 }

 // Loop through arrays and generate uniform array
 
 $arraykey = 0;
 foreach($plansList as $plan) {
 
  // Skip iteration if plan is unavailable
  if ($plan["available"] == false)
   continue;
 
  // Assign values in current iteration to table row
  $table[$arraykey] =
   $o->tablerows(
    "https://www.digitalocean.com/",
    "{$plan["price_monthly"]}.00",
    $plan["vcpus"],
    $plan["memory"],
    $plan["disk"]
   );
  if ($table[$arraykey] != 0) {
   $arraykey++;
  }

 }
 
 // Encode uniform array in JSON and write to file
 file_put_contents("public/rows/$o->providerShort.txt", json_encode($table));

}

// Rebuild regions data if regions data changed
if ($o->new["regions"]) {
 
 // Convert JSON data to array
 $oRegion->response = json_decode($oRegion->response, true);

 // Trim unnecessary information from beginning of array
 $oRegion->response = $oRegion->response["regions"];

 // Pull out only the region name
 $arraykey = 0;
 foreach($oRegion->response as $key) {
  $regionsList[$arraykey] = $key["name"];
  $arraykey++;
 }

 // Find latitude and longitude of each location
 $coords = $o->latlon($regionsList);
 
 // Encode uniform array in JSON and write to file
 file_put_contents("public/regions/$o->providerShort.txt", json_encode($coords));

}

exit(0);
?>
