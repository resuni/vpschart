<?php

/// Classes and includes

require_once("vpschart.php");
require_once("curl.php");

$o = new vpschart("Linode");

/// Download and check data

// Download plan data
$oPlan = new curl();
$oPlan->get("https://api.linode.com/v4/linode/types");
if ($oPlan->response_code != 200) exit(20);

// Download region data
$oRegion = new curl();
$oRegion->get(
 "https://api.linode.com/?api_key={$o->linodeEnv}&api_action=avail.datacenters"
);
if ($oRegion->response_code != 200) exit(20);

// Compare new data with existing data
$o->comparedata($oPlan->response, "plans");
$o->comparedata($oRegion->response, "regions");

/// Rebuild plans data if changes are detected

if ($o->new["plans"]) {

 // Convert JSON data to array
 $oPlan->response = json_decode($oPlan->response, true);
 
 // Trim unnecessary information from beginning of array
 $plansList = $oPlan->response["data"];
 
 // Loop through arrays and generate rows
 $arraykey = 0;
 foreach($plansList as $plan) {
  $table[$arraykey] =
   $o->tablerows(
    "https://www.linode.com/",
    "{$plan["price"]["monthly"]}.00",
    $plan["vcpus"],
    $plan["memory"],
    $plan["disk"]
   );
  $arraykey++;
 }
 
 // Encode uniform array in JSON and write to file
 file_put_contents("public/rows/$o->providerShort.txt", json_encode($table));

}

/// Rebuild regions data if changes are detected

if ($o->new["regions"] == 1) {

 // Convert JSON data to array
 $oRegion->response = json_decode($oRegion->response, true);

 // Trim unnecessary information from beginning and end of array
 $oRegion->response = $oRegion->response["DATA"];

 // Pull out only the region name
 $arraykey = 0;
 foreach($oRegion->response as $region) {
  $regionsList[$arraykey] = $region["ABBR"];
  $arraykey++;
 }

 // Find latitude and longitude of each location
 $coords = $o->latlon($regionsList);

 // Encode uniform array in JSON and write to file
 file_put_contents("public/regions/$o->providerShort.txt", json_encode($coords));

}

exit(0);
?>
