<?php

class curl {

 public function __construct () {
 }

 // This function produces $this->response and $this->response_code
 public function performcurl (array $opts) {
  $ci = curl_init();
  curl_setopt_array($ci, $opts);
  $this->response = curl_exec($ci);
  $this->response_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);
  curl_close($ci);
 }

 // Performs get request without API key
 public function get (string $url) {
  $opts = array(
   CURLOPT_URL => $url,
   CURLOPT_RETURNTRANSFER => true,
   CURLOPT_POST => false,
  );
  $this->performcurl($opts);
  return array(
   $this->response,
   $this->response_code
  );
 }
 
 // Performs get request with API key passed in the header
 public function keyget (string $url, string $apikey) {
  $opts = array(
   CURLOPT_URL => $url,
   CURLOPT_RETURNTRANSFER => true,
   CURLOPT_POST => false,
   CURLOPT_HTTPHEADER => array($apikey),
  );
  $this->performcurl($opts);
  return array(
   $this->response,
   $this->response_code
  );
 }
 
}

?>
