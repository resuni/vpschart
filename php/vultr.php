<?php

/// Classes and includes

require_once("vpschart.php");
require_once("curl.php");

$o = new vpschart("Vultr");

/// Download and check data

// Download plan data
$oPlan = new curl();
$oPlan->get("https://api.vultr.com/v1/plans/list");
if ($oPlan->response_code != 200) exit(20);

// Download region data
$oRegion = new curl();
$oRegion->get("https://api.vultr.com/v1/regions/list");
if ($oRegion->response_code != 200) exit(20);

// Compare new data with existing data
$o->comparedata($oPlan->response, "plans");
$o->comparedata($oRegion->response, "regions");

/// Generate table rows for Vultr if changes are detected

if ($o->new["plans"]) {

 // Convert JSON data to array
 $oPlan->response= json_decode($oPlan->response, true);
 
 // Loop through arrays and generate rows
 $arraykey = 0;
 foreach($oPlan->response as $plan) {
  $table[$arraykey] = 
   $o->tablerows(
    "https://www.vultr.com/",
    $plan["price_per_month"],
    $plan["vcpu_count"],
    $plan["ram"],
    $plan["disk"]
   );
  $arraykey++;
 }

 // Encode uniform array in JSON and write to file
 file_put_contents("public/rows/$o->providerShort.txt", json_encode($table));

}

/// Find latitude and longitute of each location if changes are detected

if ($o->new["regions"]) {

 // Convert JSON data to array
 $oRegion->response = json_decode($oRegion->response, true);
 
 // Pull out only the region name
 $arraykey = 0;
 foreach($oRegion->response as $region) {
  $regionsList[$arraykey] = $region["name"];
  $arraykey++;
 }

 // Find latitude and longitude of each location
 $coords = $o->latlon($regionsList);
 
 // Encode uniform array in JSON and write to file
 file_put_contents("public/regions/$o->providerShort.txt", json_encode($coords));

}

exit(0);
?>
